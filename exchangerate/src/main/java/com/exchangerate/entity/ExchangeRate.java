package com.exchangerate.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "exchange_rates")
@Data
@NoArgsConstructor
public class ExchangeRate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private Double value;

    public ExchangeRate(String name, Double value){
        this.name = name;
        this.value = value;
    }

    public String toString(){
        return getId() + " | " + getName() + " | " + getValue();
    }

}
