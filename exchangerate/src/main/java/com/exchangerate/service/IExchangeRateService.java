package com.exchangerate.service;

import com.exchangerate.entity.ExchangeRate;

public interface IExchangeRateService {

    Iterable<ExchangeRate> findAll();
    void deleteAll();
    ExchangeRate insert(ExchangeRate exchangeRate);
    ExchangeRate findByName(String name);
}
