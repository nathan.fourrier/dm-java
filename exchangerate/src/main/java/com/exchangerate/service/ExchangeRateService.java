package com.exchangerate.service;

import com.exchangerate.entity.ExchangeRate;
import com.exchangerate.repository.ExchangeRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ExchangeRateService implements IExchangeRateService{

    @Autowired
    private ExchangeRateRepository exchangeRateRepository;

    @Override
    public Iterable<ExchangeRate> findAll() {
        return this.exchangeRateRepository.findAll();
    }

    @Override
    public void deleteAll() {
        this.exchangeRateRepository.deleteAll();
    }

    @Override
    public ExchangeRate insert(ExchangeRate exchangeRate) {
        return this.exchangeRateRepository.save(exchangeRate);
    }

    @Override
    public ExchangeRate findByName(String name) {
        return this.exchangeRateRepository.findOneByName(name);
    }


}
