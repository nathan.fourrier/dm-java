package com.exchangerate;

import com.exchangerate.dto.ExchangeRateDto;
import com.exchangerate.entity.ExchangeRate;
import com.exchangerate.service.IExchangeRateService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Scanner;

@SpringBootApplication
@Slf4j
public class ExchangerateApplication  implements CommandLineRunner {

	@Autowired
	private IExchangeRateService exchangeRateService;

	Scanner input;

	public static void main(String[] args) {
		System.out.println("STARTING THE APPLICATION");
		ApplicationContext springApp = SpringApplication.run(ExchangerateApplication.class, args);
		System.out.println("APPLICATION FINISHED");
		SpringApplication.exit(springApp);
	}

	@Override
	public void run(String... args) {
		System.out.println("EXECUTING : command line runner");

		// TODO : Delete ?
//		for (int i = 0; i < args.length; ++i) {
//			log.info("args[{}]: {}", i, args[i]);
//		}

		this.initData();
		this.dummyExchangeRatesExamples();
		this.showMenu();
	}

	private void showMenu() {
		input = new Scanner(System.in);
		int selection = 0;

		while(selection != 4) {
			System.out.println("");
			System.out.println("Choose from these choices");
			System.out.println("-------------------------");
			System.out.println("1 - Select All From Database");
			System.out.println("2 - Show some examples");
			System.out.println("3 - Compare currencies");
			System.out.println("4 - Quit");

			selection = input.nextInt();
			System.out.println("Selection : " + selection);

			switch (selection) {
				case 1:
					this.selectAllFromDb();
					break;
				case 2:
					this.dummyExchangeRatesExamples();
					break;
				case 3:
					this.tryCurrenciesComparisons();
					break;
				case 4:
					// DO NOTHING -
					break;
				default:
					System.out.println("Unknown value. Try again.");
					this.showMenu();
					break;
			}
		}
	}

	private void tryCurrenciesComparisons() {
		Scanner inputCompare = new Scanner(System.in);
		String currency1;
		String currency2;
		ExchangeRate exchangeRate1;
		ExchangeRate exchangeRate2;

		System.out.println("Please select a currency");
		currency1 = inputCompare.next();
		exchangeRate1 = this.exchangeRateService.findByName(currency1);
		if(exchangeRate1 == null){
			System.out.println("This currency does not exist. Please try again.");
			this.tryCurrenciesComparisons();
		} else {
			System.out.println("You selected : " + exchangeRate1);
			System.out.println("Please select a second currency to compare them.");
			currency2 = inputCompare.next();
			exchangeRate2 = this.exchangeRateService.findByName(currency2);
			if(exchangeRate2 == null) {
				System.out.println("This currency does not exist. Please try again.");
				this.tryCurrenciesComparisons();
			} else {
				compareTwoCurrencies(exchangeRate1, exchangeRate2);
			}
		}

	}

	public void initData(){
		HashMap<String, Double> exchangeRate = this.getExchangeRate();
		this.clearDb();
		this.insertIntoDb(exchangeRate);
		this.selectAllFromDb();
	}

	public HashMap<String, Double> getExchangeRate(){
		RestTemplate restTemplate = new RestTemplate();
		// TODO : Add url to properties
		String getExchangeRateUrl = "https://api.exchangeratesapi.io/latest";
		ExchangeRateDto exchangeRateDto = restTemplate.getForObject(getExchangeRateUrl, ExchangeRateDto.class);

		// Add Reference currency to exchange rates
		exchangeRateDto.getRates().put(exchangeRateDto.getBase(), 1.000);

		return exchangeRateDto.getRates();
	}

	public void clearDb(){
		System.out.println("Deleting DB values...");
		exchangeRateService.deleteAll();
		System.out.println("DB cleared");
	}

	public void insertIntoDb(HashMap<String, Double> rates){
		System.out.println("");
		System.out.println("Inserting values...");
		rates.forEach((k, v) -> {
			ExchangeRate rate = new ExchangeRate(k, v);
			exchangeRateService.insert(rate);
		});
		System.out.println("Values inserted");
	}

	public void selectAllFromDb(){
		System.out.println("");
		System.out.println("Reading DB values...");
		System.out.println("id | name | value");
		exchangeRateService.findAll().forEach((exchangeRate) -> {
			//log.info("{}", exchangeRate);
			System.out.println(exchangeRate);
		});
	}

	public void dummyExchangeRatesExamples(){
		System.out.println("");
		System.out.println("Exchange rates examples :");
		ExchangeRate exchangeRate1;
		ExchangeRate exchangeRate2;

		// Case 1 : EUR and USD
		exchangeRate1 = this.exchangeRateService.findByName("EUR");
		exchangeRate2 = this.exchangeRateService.findByName("USD");
		this.compareTwoCurrencies(exchangeRate1, exchangeRate2);

		// Case 2 : USD and EUR
		exchangeRate1 = this.exchangeRateService.findByName("USD");
		exchangeRate2 = this.exchangeRateService.findByName("EUR");
		this.compareTwoCurrencies(exchangeRate1, exchangeRate2);

		// Case 3 : SEK and BGN
		exchangeRate1 = this.exchangeRateService.findByName("SEK");
		exchangeRate2 = this.exchangeRateService.findByName("BGN");
		this.compareTwoCurrencies(exchangeRate1, exchangeRate2);

		// Case 4 : SEK and BGN
		exchangeRate1 = this.exchangeRateService.findByName("ZAR");
		exchangeRate2 = this.exchangeRateService.findByName("ILS");
		this.compareTwoCurrencies(exchangeRate1, exchangeRate2);
	}

	public void compareTwoCurrencies(ExchangeRate exchangeRate1, ExchangeRate exchangeRate2){
		System.out.println("");
		System.out.println("Comparing " + exchangeRate1.getName() + " and " + exchangeRate2.getName() + " :");
		System.out.println((exchangeRate1.getValue()/exchangeRate1.getValue()) + " " + exchangeRate1.getName() + " = " +
				(exchangeRate2.getValue()/exchangeRate1.getValue()) + " " + exchangeRate2.getName());
	}

}
