package com.exchangerate.repository;

import com.exchangerate.entity.ExchangeRate;
import org.springframework.data.repository.CrudRepository;

public interface ExchangeRateRepository extends CrudRepository<ExchangeRate, Long> {
    ExchangeRate findOneByName(String name);
}
