package com.exchangerate.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.HashMap;

@Data
public class ExchangeRateDto implements Serializable {
    private long id;

    private HashMap rates = new HashMap<String, Double>();
    private String base;
    private String date;

}

